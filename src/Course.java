public class Course implements Cloneable  {
    private String subject;

    //this is method returns a shallow copy
    @Override
    public Object clone() throws CloneNotSupportedException {
        //return new Course().setSubject(subject);
        return super.clone();
    }

    public String getSubject() {
        return subject;
    }

    public Course setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String toString()
    {
        return "The Subject is : " + subject;
    }
}