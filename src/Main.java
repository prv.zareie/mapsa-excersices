public class Main {
    public static void main(String[] args) {
        try {
            System.out.println("*********************** BOOK Section ***************************");
            Book book = new Book("Paiting", "Art", "Bob Rush");
            Book clonedBook = (Book) book.clone();
            System.out.println(book.toString());
            System.out.println(clonedBook.toString());
            book.setAuthor("Bob Rush 2!");
            System.out.println("After Changing :");
            System.out.println(book.toString());
            System.out.println(clonedBook.toString());

            System.out.println("*********************** COURSE Section ***************************");
            Course course = new Course();
            course.setSubject("First Subject");
            Course clonedCourse = (Course) course.clone();
            System.out.println(course.toString());
            System.out.println(clonedCourse.toString());
            course.setSubject("New Subject");
            System.out.println("After Changing :");
            System.out.println(course.toString());
            System.out.println(clonedCourse.toString());

            System.out.println("*********************** TEACHER Section ***************************");
            Teacher teacher = new Teacher("Pari Zarei",course,book);
            Teacher clonedTeacher = teacher.clone();
            System.out.println(teacher.toString());
            System.out.println(clonedTeacher.toString());
            teacher.setCourse(clonedCourse);
            System.out.println("After Changing :");
            System.out.println(teacher.toString());
            System.out.println(clonedTeacher.toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }
}
